# Forecast Plus App v0.0.1

A simple App using AngularJS, SASS, LocalStorage and OpenWeatherMap API.

Live version available at http://868184.beats01.web.hosting-test.net

## Overview

This app is for tourists who have not decided on the city for the next trip.

On the home page there are randomly selected popular destinations.

The color of the city cards depends on the weather of each city.

By clicking on the city on the right hand-side the user is shown the weather forecast for this city during the 4 following days.

User can add the city he has liked into the personal list to compare forecasts by clicking on ❤️ icon.

## Development Instructions

1. Install all components: `npm install`
2. Start the local web server: `npm start`
3. Use your local SASS compiler
4. Open a new browser tab and go to: http://localhost:8000/